# Spatiotemporal single cell analysis of human colonic macrophages
### Victoria Therese Karlsen
##### University of Oslo, 2021

The adult human gut spans an area of about 200–400 m2, and this huge surface is covered by a single layer of epithelial cells creating a physical barrier against the environment. To maintain the integrity of this critical interface, it is crucial for the immune system to provide protection against intestinal pathogens, while at the same time avoiding excessive immune responses. Failure of intestinal immune regulation may lead to severe and debilitating chronic inflammatory disorders, like inflammatory bowel disease (IBD). Maintenance of homeostasis in the intestinal tract requires a critical regulation of host immune responses to the luminal contents, and intestinal macrophages are essential in this process by their production of immunoregulatory factors and potential to propagate regulatory T cells (Tregs) in the gut. 

However, most of our knowledge to this end is derived from experimental animal models and in vitro cell cultures, and studies of human tissues are highly warranted. Moreover, most experimental, and clinical results from tissues are based on bulk-analysis of cells or tissue samples. Such “averaging” of data inadvertently misses critical information because the heterogeneity of the samples is ignored, while the nature of biology is highly diverse. 

To fully capture the heterogeneity and functional capacities of intestinal macrophages, we performed an unbiased single-cell transcriptomic analysis of macrophages isolated from human colon. This study shows that the colon mucosa contains monocyte-like cells that appear to differentiate into multiple transcriptionally distinct subsets, including subsets with proinflammatory properties and subsets with high antigen presenting and phagocytic capacity. Moreover, the various macrophage subtypes appeared to occupy distinct niches in the tissues. Detailed understanding of the intestinal macrophage landscape in healthy tissues may guide development of new anti-inflammatory treatment approaches.


This folder contians the scripts used for the Seurat and Monocle 3 analyses performed in the master thesis _Spatiotemporal single cell analysis of human colonic macrophages_. 

