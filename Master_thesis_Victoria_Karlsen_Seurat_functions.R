# A function for reading in a 10x dataset
## Returned: the loaded dataset
### dataset = a variable containing the path to the dataset which should be read
readData <- function(dataset)
{
  Read10X(data.dir = dataset)
}

# A function filtering the patient data, removing mitochondrial genes, keeping cells withing a specified range of RNA count and mitochondrial genes
## Returned: plots of the quality control and features before and after removing miochondrial genes, in addition to filtered data where only cells within the set perameters are included 
### sampleData = a variable containing the patient data 
### nfeaturesMin = a variable containing a number for the minumum number of features to include in the analysis
### nfeaturesMax = a variable containing a number for the maximum number of features to include in the analysis
### pathToPlots = a variable containing the path to the folder in which the plots should be saved
### sampleId = a variable containing the patient number (e.g. mucosa_p1)
### extraParameter = ?
### howMuchMT = a variable containing a number for the maximum percentage of mitochondrial genes to include in the analysis
plotCleanDataAll <- function(sampleData, nfeaturesMin, nfeaturesMax, pathToPlots, sampleId, extraParameter, howMuchMT) {
  
  sampleData[["percent.mt"]] <- PercentageFeatureSet(sampleData, pattern = "^MT-")
  sampleData[["percent.rp"]] <- PercentageFeatureSet(sampleData, pattern = "^RP[SL]")
  
  fName <- paste("QCmetrics-before-", sampleId, ".pdf", sep="")
  pdf(paste(pathToPlots,fName, sep=""), 9, 5) 
  print(VlnPlot(sampleData, features = c("nFeature_RNA", "nCount_RNA", "percent.mt", "percent.rp"), ncol = 2))
  dev.off()
  
  fName <- paste("Relationship_between-features-before-", sampleId, ".pdf", sep="")
  plot1 <- FeatureScatter(sampleData, feature1 = "nCount_RNA", feature2 = "percent.mt")
  plot2 <- FeatureScatter(sampleData, feature1 = "nCount_RNA", feature2 = "percent.rp")
  plot3 <- FeatureScatter(sampleData, feature1 = "nCount_RNA", feature2 = "nFeature_RNA")
  pdf(paste(pathToPlots,fName, sep=""), 9, 5) 
  
  txt1 = paste("Number of cells (BEFORE removing MT): ",  dim(sampleData)[2] , sep="")
  txt2 = paste("Number of genes (BEFORE removing MT): ",  dim(sampleData)[1] , sep="")
  
  print(plot1 + plot2 + plot3)
  dev.off()
  
  
  sampleData <- subset(sampleData, subset = nFeature_RNA > nfeaturesMin & nFeature_RNA < nfeaturesMax & percent.mt <= as.integer(howMuchMT))
  
  fName <- paste("QCmetrics-after-", sampleId, ".pdf", sep="")
  pdf(paste(pathToPlots,fName, sep=""), 9, 5) 
  print(VlnPlot(sampleData, features = c("nFeature_RNA", "nCount_RNA", "percent.mt", "percent.rp"), ncol = 2))
  dev.off()
  
  
  fName <- paste("Relationship_between-features-after", sampleId, ".pdf", sep="")
  plot1 <- FeatureScatter(sampleData, feature1 = "nCount_RNA", feature2 = "percent.mt")
  plot2 <- FeatureScatter(sampleData, feature1 = "nCount_RNA", feature2 = "nFeature_RNA")
  pdf(paste(pathToPlots,fName, sep=""), 9, 5) 
  print(CombinePlots(plots = list(plot1, plot2)))
  dev.off()
  
  txt3 = paste("Number of cells (AFTER removing MT): ",  dim(sampleData)[2] , sep="")
  txt4 = paste("Number of genes (AFTER removing MT): ",  dim(sampleData)[1] , sep="")
  
  
  return(sampleData)
}

# A function where first, a seurat object is created, then the data is filtered (removing mitochondrial genes) and normalized. Then variable festures are identified 
## Returned: a seurat object of the patient data, which has been filtered and normalized with variable features identified 
### patientData = a variable containing the patient data 
### sampleId = a variable containing the patient number (e.g. mucosa_p1)
createAndCleanSeurat <- function(patientData, sampleId)
{
  nfeatures = 2000
  nfeaturesMin = 200
  nfeaturesMax = 4000
  minCells = 3
  minReads = 200
  howMuchMT = 5
  scaleFactor = 10000
  
  mucosa_p1 <- CreateSeuratObject(counts = patientData, project = sampleId, min.cells = minCells, min.features = minReads)
  mucosa_p1 <- plotCleanDataAll(mucosa_p1, nfeaturesMin, nfeaturesMax, analysisPath,sampleId, "", howMuchMT)
  mucosa_p1 <- NormalizeData(mucosa_p1, normalization.method = "LogNormalize", scale.factor = scaleFactor)
  mucosa_p1 <- FindVariableFeatures(mucosa_p1, selection.method = "vst", nfeatures = nfeatures)
  saveRDS(mucosa_p1, file =  paste(technicalPath, sampleId, "-sampleData", ".rds", sep=""))
  
  return(mucosa_p1)
}

# A function finding which dimensions to use for further analysis
## Returned: the correct number of dimensions to use as well as a plot visualizing the differnt dims   
### integratedSampleData = a variable containing the integrated sample data from which the correct dimensions should be found
### dataType = a variable specifying if the data is before or after removing clusters 
findCorrectDims <- function(integratedSampleData, dataType){
  pct <- integratedSampleData[["pca"]]@stdev / sum(integratedSampleData[["pca"]]@stdev) * 100
  cumu <- cumsum(pct)
  co1 <- which(cumu > 90 & pct < 5)[1]
  co2 <- sort(which((pct[1:length(pct) - 1] - pct[2:length(pct)]) > 0.1), decreasing = T)[1] + 1
  pcs <- min(co1, co2)
  plot_df <- data.frame(pct = pct, cumu = cumu, rank = 1:length(pct))
  
  fName <- paste("Plot_of_Dims",dataType,".pdf", sep="")
  pdf(paste(analysisPath,fName, sep=""), 9, 5) 
  print(ggplot(plot_df, aes(cumu, pct, label = rank, color = rank > pcs)) + geom_text() + geom_vline(xintercept = 90, color = "grey") + geom_hline(yintercept = min(pct[pct > 5]), color = "grey") +theme_bw())
  dev.off()
  
  return(pcs)
}

# A function creating a clustering tree
## Returend: different resolutions for the integrated data 
### combinedData = a variable containing the integrated sample data from which the culstering tree should be generated
differentResClusTreeDIMS <- function(combinedData)
{
  for (r in 1:20)
  {
    res <- r/10
    combinedData <- FindClusters(combinedData, verbose=FALSE, resolution = res)
  }
  return(combinedData)
}

# A function finding all positive markers in each cluster
## Returned: an exel file with all positive markers in each cluster
### combinedData = a variable containing the integrated sample data from which the positive markers should be found
### res = the resolution used for the clustering 
### dataType = a variable specifying if the data is before or after removing clusters 
AllPositiveMarkers <- function(combinedData, res, dataType){
  minPercentage = 0.15 
  treshold = 0.25
  
  mucosaMarkers <- FindAllMarkers(combinedData, only.pos = TRUE, min.pct = minPercentage, logfc.threshold = treshold)
  sm <- mucosaMarkers %>% group_by(cluster) 
  markersOrderedByRNA <- sm[order(sm$cluster, -sm$avg_logFC),]
  saveRDS(mucosaMarkers, file = paste0(analysisPath, "mucosaMarkersIntegrated(sm)_res=", res, dataType, ".rds"))
  fName = paste("MucosaPositiveMarkersForAllClusters_res=", res, dataType, ".xlsx", sep = "")
  write.xlsx(sm[order(sm$cluster, -sm$avg_logFC),], file = paste(analysisPath, fName, sep=""), row.names=T)
  
  return(markersOrderedByRNA)
}

# A function generating feature plots of selected genes 
## Returned: feature plots
### combinedData = a variable containing the integrated sample data from which the selected genes should be found
### listOfGenesToPlot = a variable containing a list of the gene names which should be plotted 
### typeOfGenes = a variable containing information about the type of genes being plottet (e.g. mature macrophage genes)
geneFeaturePlot <- function(combinedData, listOfGenesToPlot, typeOfGenes)
{
  mucosaFeatPlot <- FeaturePlot(combinedData, listOfGenesToPlot ,reduction = "umap",label = TRUE)
  
  fName <- paste("FeaturePlots_MucosaIntegrated_res=0.8_AfterRemovingClusters", typeOfGenes, ".pdf", sep="")
  pdf(paste(analysisPath,fName, sep=""), 10, 10) 
  print(mucosaFeatPlot)
  dev.off()
  
  return(mucosaFeatPlot)
}

# A function for saving PDFs 
## Returned: a pdf file of the saved object 
### figVarName = the name of the variable created to save the pdf, e.g. fig1
### path = the path you would like to use for saving in the right folder
### fileName = the name you would like to give the pdf
### plotName = the name of the plot you would like to save
### howWide = the width of the pdf
### howHigh = the height of the pdf

PDFsave <- function(figVarName, path, fileName, plotName, howWide, howHigh)
{
  figVarName = paste(path, fileName, sep = "")
  pdf(figVarName, howWide, howHigh)
  print(plotName)
  dev.off()
}

